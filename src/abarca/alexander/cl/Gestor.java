package abarca.alexander.cl;

import java.time.LocalDate;
import java.util.ArrayList;
import static java.lang.System.out;

/**
 * @author Alexander
 * @version 1.0
 * Clase que representa el gestor de la aplicación.
 */
public class Gestor {
    private ArrayList<Cliente> clientes = new ArrayList<Cliente>();
    private ArrayList<Cuenta> cuentas = new ArrayList<Cuenta>();
    private Movimiento movimiento = new Movimiento();

    /**
     * Metodo que permite registrar un cliente en la aplicación.
     * @param pnombre Nombre del cliente.
     * @param papellido Apellido del cliente.
     * @param pdireccion Dirección del cliente.
     * @param pfechaNacimiento Fecha de nacimiento del cliente.
     * @param pid Identificador del cliente.
     * @return true si el cliente se registro correctamente, false si no se registro.
     */
    public boolean registrarCliente(String pnombre, String papellido, String pdireccion, LocalDate pfechaNacimiento, int pid){
        Cliente cliente = new Cliente(pnombre, papellido, pdireccion, pfechaNacimiento, pid);
        int idCliente = 0;
        boolean registro = false;
        ArrayList<Cliente> clientes = listarClientes();
        for (Cliente cliente2 : clientes) {
            idCliente = cliente2.getId();
        }
        if(idCliente == cliente.getId()){
            out.println("Operación No realizada!. Ya existe un cliente con ese numero");
        }
        else{
            clientes.add(cliente);
            registro = true;
        }
        return registro;
    }

    /**
     * Metodo que permite listar los clientes registrados en la aplicación.
     * @return ArrayList de clientes.
     */
    public ArrayList<Cliente> listarClientes(){
        return this.clientes;
    }

    /**
     * Metodo que permite listar las cuentas registradas en la aplicación.
     * @return ArrayList de cuentas.
     */

    public ArrayList<Cuenta> listarCuentas(){
        return this.cuentas;
    }

    /**
     * Metodo que permite verificar si un cliente existe
     * @param pid Identificador del cliente.
     * @return true si el cliente existe, false si no existe.
     */
    public boolean existeCliente(int pid){
        for(Cliente cliente: clientes){
            if(cliente.getId() == pid){
                return true;
            }
        }
        return false;
    }

    /**
     * Metodo que permite registrar una cuenta en la aplicación.
     * @param pidCliente Identificador del cliente.
     * @param psaldo Saldo de la cuenta.
     * @ptipo Tipo de cuenta.
     * @return true si la cuenta se registro correctamente, false si no se registro.
     */
    public boolean registrarCuenta(int pidCliente,  double psaldo, int ptipo, double pmonto, int pdia, int pcuentaAsociada){
        Cuenta cuenta = new CuentaCorriente();
        boolean registro = false;
        boolean flag = false;
        switch(ptipo){
            case 1:
                cuenta = new CuentaCorriente(pidCliente, psaldo, ptipo);
                break;
            case 2:
                cuenta = new CuentaAhorro(pidCliente, psaldo, ptipo);
                break;
            case 3:
                cuenta = new CuentaAhorroProgramada(pidCliente, psaldo, ptipo, pmonto, pdia, pcuentaAsociada);
                break;
        }
        ArrayList<Cuenta> cuentas = listarCuentas();
        for (Cuenta cuenta2 : cuentas) {
            if(cuenta2.getNumeroCuenta() == cuenta.getNumeroCuenta()) {
                out.println("Operación No realizada!. Ya existe una cuenta con ese numero");
                flag = true;
            }
        }
        if(!flag){
            cuentas.add(cuenta);
            registro = true;
        }
        return registro;
    }

    /**
     * Metodo que permite buscar una cuenta en la aplicación.
     * @param pidCliente Identificador del cliente.
     * @return Cuenta encontrada.
     */
    public ArrayList<Cuenta> buscarCuenta(int pidCliente) {
        ArrayList<Cuenta> listaCuentasEncontradas = new ArrayList<Cuenta>();
        boolean flag = false;
        for (Cuenta cuenta : cuentas) {
            if (cuenta.getIdCliente() == pidCliente) {
                listaCuentasEncontradas.add(cuenta);
                flag = true;
            }
        }
        if(!flag){
            out.println("No se encontró cuenta");
        }
        return listaCuentasEncontradas;
    }

    /**
     * Metodo que permite depositar dinero en una cuenta.
     * @param pidCuenta Identificador de la cuenta.
     * @param pdeposito Cantidad de dinero a depositar.
     */
    public void depositar(int pidCuenta, double pdeposito, String descripcion){
        for(Cuenta cuenta: cuentas){
            if(cuenta.getNumeroCuenta() == pidCuenta){
                double saldoInicial = cuenta.getSaldo();
                double newSaldo = movimiento.depositar(pdeposito, saldoInicial, descripcion);
                cuenta.setSaldo(newSaldo);
                out.println("Depósito realizado el dia " + LocalDate.now() + "\ncon un monto de " + pdeposito + " colones" + "\npor motivo de: "+descripcion +"\ny su nuevo saldo es de " + cuenta.getSaldo() + " colones");
            }
        }
    }

    /**
     * Metodo que permite retirar dinero de una cuenta.
     * @param pidCuenta Identificador de la cuenta.
     * @param pretiro Cantidad de dinero a retirar.
     */
    public void retirar(int pidCuenta, double pretiro, String descripcion){
        for(Cuenta cuenta: cuentas){
            if(cuenta.getNumeroCuenta() == pidCuenta){
                double saldoInicial = cuenta.getSaldo();
                double newSaldo = movimiento.retirar(pretiro, saldoInicial, descripcion);
                cuenta.setSaldo(newSaldo);
                out.println("\nRetiro realizado el dia " + LocalDate.now() + "\npor un monto de " + pretiro + " colones" + "\npor motivo de: "+descripcion +"\ny su nuevo saldo es de " + cuenta.getSaldo() + " colones");
            }
        }
    }

    /**
     * Método que permite verificar si una cuenta existe.
     * @param pidCuenta Identificador de la cuenta.
     * @return true si la cuenta existe, false si no existe.
     */
    public boolean existeCuenta(int pidCuenta){
        for(Cuenta cuenta: cuentas){
            if(cuenta.getNumeroCuenta() == pidCuenta){
                return true;
            }
        }
        return false;
    }

    /**
     * Método para devolver el tipo de cuenta.
     * @param pidCuenta Identificador de la cuenta.
     * @return Tipo de cuenta.
     */
    public String tipoCuenta(int pidCuenta){
        for(Cuenta cuenta: cuentas){
            if(cuenta.getNumeroCuenta() == pidCuenta){
                return cuenta.getTipoCuenta();
            }
        }
        return null;
    }

    /**
     * Método para devolver el saldo de una cuenta.
     */
    public void actualizarSaldoAhorro(){
        for(Cuenta cuenta: cuentas){
            if(cuenta.getTipoCuenta().equals("Ahorro")){
                CuentaAhorro cuentaAhorro = (CuentaAhorro) cuenta;
                if(LocalDate.now().getDayOfMonth() == 10 && cuentaAhorro.isActualizado() == false){
                    cuenta.setSaldo(cuenta.getSaldo() + cuenta.getSaldo() * CuentaAhorro.getInteres());
                    out.println("Hoy las Cuentas de ahorro han recibido un interés de " + CuentaAhorro.getInteres() * 100 + "%");
                    out.println("Su nuevo saldo es de " + cuenta.getSaldo() + " colones");
                    cuentaAhorro.setActualizado(true);
                }
            }
        }
    }

    /**
     * Método para comprobar una cuenta de ahorro programada
     * @param pidCuenta Identificador de la cuenta.
     * @return true si la cuenta es de ahorro programada, false si no es de ahorro programada.
     */
    public boolean comprobarCuentaAhorroProgramada(int pidCuenta){
        boolean result = false;
        if(existeCuenta(pidCuenta)){
            if(tipoCuenta(pidCuenta).equals("Corriente")){
                result = true;
            }else {out.println("La cuenta no es de tipo corriente");}
        }else{out.println("La cuenta no existe");}
        return result;
    }

    /**
     * Método para actualizar el ahorro programado
     */
    public void actualizarAhorroProgramado(){
        for(Cuenta cuenta: cuentas){
            if(cuenta.getTipoCuenta().equals("Ahorro Programado")){
                CuentaAhorroProgramada cuentaAhorroProgramada = (CuentaAhorroProgramada) cuenta;
                if(LocalDate.now().getDayOfMonth() == cuentaAhorroProgramada.getDia() && cuentaAhorroProgramada.isActualizado() == false){
                    retirar(cuentaAhorroProgramada.getCuentaAsociada(), cuentaAhorroProgramada.getMonto(), "Ahorro Programado");
                    cuenta.setSaldo(cuenta.getSaldo() + cuentaAhorroProgramada.getMonto());
                    out.println("\nAhorro programado realizado con éxito, su nuevo saldo es de " + cuenta.getSaldo() + " colones");
                    cuentaAhorroProgramada.setActualizado(true);
                }
            }
        }
    }
}


