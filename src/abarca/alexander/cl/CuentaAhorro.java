package abarca.alexander.cl;

/**
 * @author abarca
 * @version 1.0
 * @see CuentaAhorro
 * @descripcion Clase que representa una cuenta de ahorro
 */
public class CuentaAhorro extends Cuenta {
    private static double interes = 0.05;
    protected boolean actualizado = false;

    /**
     * @descripcion Constructor de la clase CuentaAhorro
     * @param idCliente Identificador del cliente
     * @param saldo Saldo de la cuenta
     * @param tipo  Tipo de cuenta
     */
   public CuentaAhorro(int idCliente, double saldo, int tipo) {
        super(idCliente, saldo, tipo);
    }

    /**
     * @descripcion Método que actualiza el saldo de la cuenta
     * @param interes Interes de la cuenta
     */
    public static void setInteres ( double interes){
       CuentaAhorro.interes= interes;
   }

    /**
     * @descripcion Método que devuelve el interés de la cuenta
     * @return  Retorna el saldo de la cuenta
     */
   public static double getInteres () {
       return CuentaAhorro.interes;
   }

    /**
     * @descripcion Método para conocer si el saldo de la cuenta fue actualizado
     * @return Retorna el valor de actualizado
     */
    public boolean isActualizado() {
        return actualizado;
    }

    /**
     * @descripcion Método indicar que el saldo de la cuenta fue actualizado
     * @param actualizado Valor de actualizado
     */
    public void setActualizado(boolean actualizado) {
        this.actualizado = actualizado;
    }
}

