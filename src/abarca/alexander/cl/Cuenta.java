package abarca.alexander.cl;

import java.time.LocalDate;

/**
 * @author Alexander Abarca
 * Clase que representa una cuenta bancaria
 * @version 1.0
 */
public abstract class Cuenta {
    private int numeroCuenta;
    private double saldo;
    private int idCliente;
    private String tipoCuenta;
    private LocalDate fechaApertura;

    /**
     * Constructor vacio de la clase
     */
    public Cuenta() {
        this.numeroCuenta = 0;
        this.saldo = 0;
        this.idCliente = 0;
        this.tipoCuenta = "";
        this.fechaApertura = null;
    }
    /**
     * Constructor de la clase
     * @param saldo saldo de la cuenta
     * @param idCliente id del cliente
     */
    public Cuenta(int idCliente, double saldo, int tipo) {
        this.idCliente = idCliente;
        this.fechaApertura = LocalDate.now();
        this.numeroCuenta = (int) (Math.random() * 10000000);
        this.saldo = saldo;
        if (tipo == 1) {
            this.tipoCuenta = "Corriente";
        }
        else if (tipo == 2) {
            this.tipoCuenta = "Ahorro";
        }
        else {this.tipoCuenta = "Ahorro Programado";}
        System.out.println("Cuenta: " + this.tipoCuenta + ", Numero de cuenta: " + this.numeroCuenta);
    }

    /**
     * Método que retorna el número de cuenta
     * @return numero de cuenta
     */
    public int getNumeroCuenta() {
        return numeroCuenta;
    }

    /**
     * Método que retorna el saldo de la cuenta
     * @return saldo de la cuenta
     */
    public double getSaldo() {
        return saldo;
    }

    /**
     * Método que modifica el saldo de la cuenta
     * @param saldo saldo de la cuenta
     */
    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    /**
     * Método que retorna el saldo de la cuenta
     * @param numeroCuenta numero de cuenta
     */
    public void setNumeroCuenta(int numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    /**
     * Método que retorna el id del cliente
     * @return id del cliente
     */
    public int getIdCliente() {
        return idCliente;
    }

    /**
     * Método que modifica el id del cliente
     * @param idCliente id del cliente
     */
    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    public LocalDate getFechaApertura() {
        return fechaApertura;
    }

    public void setFechaApertura(LocalDate fechaApertura) {
        this.fechaApertura = fechaApertura;
    }


    /**
     * Método que permite consultar el saldo de la cuenta
     * @return saldo de la cuenta
     */
    @Override
    public String toString() {
        return "Cuenta{tipo: " + tipoCuenta + ", numeroCuenta = " + numeroCuenta + ", saldo = " + saldo + ", apertura: " +fechaApertura + '}';
    }
}
