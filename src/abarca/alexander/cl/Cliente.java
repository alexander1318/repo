package abarca.alexander.cl;
import java.time.LocalDate;
import java.time.Period;

/**
 * @author Alexander
 * @version 1.0
 */
public class Cliente {
    private String nombre;
    private String apellido;
    private String direccion;
    private LocalDate fechaNacimiento;
    private int edad;
    private int id;

    /**
     * Constructor vacío de la clase cliente
     */
    public Cliente() {
        this.nombre = null;
        this.apellido = null;
        this.direccion = null;
        this.fechaNacimiento = LocalDate.now();
        this.id = 0;
        this.edad = Period.between(fechaNacimiento, LocalDate.now()).getYears();
    }

    /**
     * Constructor con parámetros de la clase cliente
     * @param nombre Nombre del cliente
     * @param apellido Apellido del cliente
     * @param direccion Dirección del cliente
     * @param fechaNacimiento Fecha de nacimiento del cliente
     * @param id Identificador del cliente
     */
    public Cliente(String nombre, String apellido, String direccion, LocalDate fechaNacimiento, int id) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.direccion = direccion;
        this.fechaNacimiento = fechaNacimiento;
        this.id = id;
        this.edad = Period.between(fechaNacimiento, LocalDate.now()).getYears();
    }

    /**
     * Método que devuelve el nombre del cliente
     * @return Nombre del cliente
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Método que permite modificar el nombre del cliente
     * @param nombre Nuevo nombre del cliente
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Método que obtiene el apellido del cliente
     * @return Apellido del cliente
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * Método que permite modificar el nombre del cliente
     * @param apellido Nuevo apellido del cliente
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * Método que devuelve la dirección del cliente
     * @return Dirección del cliente
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Método que permite modificar la dirección del cliente
     * @param direccion Nueva dirección del cliente
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * Método que devuelve la fecha de nacimiento del cliente
     * @return Fecha de nacimiento del cliente
     */
    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     * Método que permite modificar la fecha de nacimiento del cliente
     * @param fechaNacimiento Nueva fecha de nacimiento del cliente
     */
    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    /**
     * Método que devuelve la edad del cliente
     * @return Edad del cliente
     */
    public int getEdad() {
        return edad;
    }

    /**
     * Método que permite modificar la edad del cliente
     * @param edad Nueva edad del cliente
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * Método que devuelve el identificador del cliente
     * @return Identificador del cliente
     */
    public int getId() {
        return id;
    }

    /**
     * Método que permite modificar el identificador del cliente
     * @param id Nuevo identificador del cliente
     */
    public void setId(int id) {
        this.id = id;
    }
}
