package abarca.alexander.cl;
import java.time.LocalDate;

public class Movimiento {
   private LocalDate fecha;
   private String descripcion;

    /**
     * Método que deposita una cantidad en la cuenta
     * @param cantidad cantidad a depositar
     */
    public double depositar(double cantidad, double saldoInicial, String descripcion) {
        this.descripcion = descripcion;
        double saldoFinal = cantidad + saldoInicial;
        return saldoFinal;
    }

    /**
     * Método que permite el retiro de una cantidad de la cuenta
     * @param cantidad cantidad a retirar
     */
    public double retirar(double cantidad, double saldoInicial, String descripcion) {
        this.descripcion = descripcion;
        double saldoFinal = saldoInicial - cantidad;
        return saldoFinal;
    }
}
