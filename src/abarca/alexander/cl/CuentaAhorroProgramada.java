package abarca.alexander.cl;

/**
 * @author abarca
 * @version 1.0
 * Clase CuentaAhorroProgramada
 */
public class CuentaAhorroProgramada extends Cuenta {
    protected double monto;
    protected int dia;
    protected int cuentaAsociada;
    protected boolean actualizado = false;


    public CuentaAhorroProgramada(){
        super();
        this.monto = 0;
        this.dia = 0;
        this.cuentaAsociada = 0;
    }

    /**
     * Metodo para obtener el saldo de la cuenta
     * @param idCliente id del cliente
     * @param saldo saldo de la cuenta
     * @param tipo tipo de cuenta
     * @param monto monto a ahorrar
     * @param dia dia en que se ahorra
     * @param cuentaAsociada cuenta a la que se asociará
     */
    public CuentaAhorroProgramada(int idCliente, double saldo, int tipo, double monto, int dia, int cuentaAsociada) {
        super(idCliente, saldo, tipo);
        this.monto = monto;
        this.dia = dia;
        this.cuentaAsociada = cuentaAsociada;
    }

    /**
     * Metodo para obtener el monto a ahorrar
     * @return monto a ahorrar
     */
    public double getMonto() {
        return this.monto;
    }

    /**
     * Metodo para actualizar el saldo de la cuenta
     * @param monto monto a ahorrar
     */
    public void setMonto(double monto) {
        this.monto = monto;
    }

    /**
     * Metodo para obtener el dia en que se ahorra
     * @return dia en que se ahorra
     */
    public int getDia() {
        return this.dia;
    }

    /**
     * Metodo para actualizar el día en que se ahorra
     * @param dia dia en que se ahorra
     */
    public void setDia(int dia) {
        this.dia = dia;
    }

    /**
     * Metodo para conocer si el saldo de la cuenta está actualizado
     * @return true si está actualizado, false si no
     */
    public boolean isActualizado() {
        return actualizado;
    }

    /**
     * Metodo para decir que el saldo de la cuenta fue actualizado
     * @param actualizado true si está actualizado, false si no
     */
    public void setActualizado(boolean actualizado) {
        this.actualizado = actualizado;
    }

    /**
     * Metodo para obtener la cuenta a la que se asociará
     * @return cuenta a la que se asocia
     */
    public int getCuentaAsociada() {
        return cuentaAsociada;
    }

    /**
     * Metodo para actualizar la cuenta a la que se asociará
     * @param cuentaAsociada cuenta a la que se asociará
     */
    public void setCuentaAsociada(int cuentaAsociada) {
        this.cuentaAsociada = cuentaAsociada;
    }
}
