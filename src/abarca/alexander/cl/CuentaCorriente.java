package abarca.alexander.cl;

import java.util.ArrayList;

import static java.lang.System.out;

/**
 * @author Alexander Abarca
 * @version 1.0
 * Clase CuentaCorriente
 */
public class CuentaCorriente extends Cuenta {

    public CuentaCorriente() {
    }
/**
 * @param idCliente id del cliente
 * @param saldo saldo de la cuenta
 * @param tipo tipo de cuenta
 */
    public CuentaCorriente(int idCliente, double saldo, int tipo) {
        super(idCliente, saldo, tipo);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}

