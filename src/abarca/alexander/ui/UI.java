package abarca.alexander.ui;
import java.io.*;
import java.util.ArrayList;
import java.time.LocalDate;
import abarca.alexander.cl.Cuenta;
import abarca.alexander.cl.Cliente;
import abarca.alexander.cl.Gestor;

import static java.lang.System.out;

/**
 * Clase que contiene los métodos para la interfaz gráfica
 * @author Alexander Abarca
 * @version 1.0
 */
public class UI {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static Gestor gestor = new Gestor();

    /**
     * Rutina principal
     * @param args argumentos de la línea de comandos
     * @throws IOException excepción de entrada y salida
     */
    public static void main(String[] args) throws IOException{

        int opcion= -1;
        inicializarPrograma();

        do{
            gestor.actualizarSaldoAhorro();
            gestor.actualizarAhorroProgramado();
            mostrarMenu();
            opcion = seleccionarOpcion();
            procesarOpcion(opcion);
        }while (opcion!=0);
    }

    /**
     * Rutina para mostrar el menú
     */
    public static void mostrarMenu(){
        out.println("\nMenú Principal");
        out.println("==============\n");
        out.println("\t1. Registrar cliente");
        out.println("\t2. Listar clientes");
        out.println("\t3. Crear cuenta");
        out.println("\t4. Realizar depósito");
        out.println("\t5. Realizar retiro");
        out.println("\t6. Consultar saldo");
        out.println("\t0. Salir");
    }

    /**
     * Rutina que permite procesar la opcion seleccionada por el usuario
     * @param pOpcion opcion seleccionada por el usuario
     * @throws IOException excepcion de entrada y salida
     */
    public static void procesarOpcion(int pOpcion) throws IOException {
        switch (pOpcion){
            case 1:
                registroCliente();
                break;
            case 2:
                imprimirClientes();
                break;
            case 3:
                crearCuenta();
                break;
            case 4:
                realizarDeposito();
                break;
            case 5:
                realizarRetiro();
                break;
            case 6:
                mostrarCuentas();
                break;
            case 0:
                out.println("\nSaliendo... Gracias por usar el programa");
                System.exit(0);
                break;
            default:
                out.println("Opción inválida");
                break;
        }
    }

    /**
     * Rutina para mostrar mensaje de bienvenida
     */
    public static void inicializarPrograma(){
        out.println("Banco los Lavanderos");
        out.println("====================\n");
        out.println("Bienvenido, ¿qué desea hacer?:");
    }

    /** Rutina para seleccionar una opcion del menu
     * @return opcion seleccionada por el usuario
     * @throws IOException excepcion de entrada y salida
     */
    public static int seleccionarOpcion() throws IOException{
        out.print("\nDigite la opción >> ");
        return Integer.parseInt(in.readLine());
    }
    /**
     * Rutina para registrar un cliente
     * @throws IOException excepción de entrada y salida
     */
    public static void registroCliente() throws IOException{
        out.println("\nRegistrar cliente");
        out.println("================");
        out.println("\nDigite el nombre del cliente:");
        String nombre = in.readLine();
        out.println("\nDigite el apellido del cliente:");
        String apellido = in.readLine();
        out.println("\nDigite el número de documento (id) del cliente:");
        int id = Integer.parseInt(in.readLine());
        out.println("\nDigite la dirección del cliente:");
        String direccion = in.readLine();
        out.println("\nDigite la fecha de nacimiento (yyyy-mm-dd) del cliente:");
        LocalDate fechaNacimiento = LocalDate.parse(in.readLine());
        boolean registro = gestor.registrarCliente(nombre, apellido, direccion, fechaNacimiento, id);
        if(registro) {
            out.println("\nCliente registrado con éxito");
        }
    }

    /**
     * Rutina para listar los clientes
     * @throws IOException excepción de entrada y salida
     */
    public static void imprimirClientes() throws IOException{
        out.println("\nListado de clientes");
        out.println("==================\n");
        ArrayList<Cliente> clientes = gestor.listarClientes();
        out.printf("%-15s\t%-15s\t%-15s\t%-15s\t%-15s\t%-15s\n\n","identificación","Nombre","Apellidos", "Dirección", "Edad", "Cuentas");
        for(Cliente cliente : clientes){
            //Asignamos 15 espacios justificados a la izquierda a cada string y separamos con un tabulador
            out.printf("%-15d\t%-15s\t%-15s\t%-15s\t%-15d\t%-15s\n",cliente.getId(), cliente.getNombre(), cliente.getApellido(), cliente.getDireccion(), cliente.getEdad(), gestor.buscarCuenta(cliente.getId()).toString());
        }
    }

    /**
     * Rutina para crear una cuenta corriente
     * @throws IOException excepción de entrada y salida
     */
    public static void crearCuenta() throws IOException{
        out.println("\nCrear cuenta");
        out.println("=================");
        out.println("\nDigite el número de documento (id) del cliente:");
        int idCliente = Integer.parseInt(in.readLine());
        if(gestor.existeCliente(idCliente)){
            out.println("\nDigite el tipo de cuenta (1: Cuenta Corriente, 2: Cuenta Ahorro, 3: Ahorro Programado):");
            int tipoCuenta = Integer.parseInt(in.readLine());
            if(tipoCuenta == 1 || tipoCuenta == 2 ) {
                out.println("\nDigite el saldo inicial:");
                double saldo = Double.parseDouble(in.readLine());
                if (saldo >= 50000) {
                    boolean registro = gestor.registrarCuenta(idCliente, saldo, tipoCuenta,0, 0, 0);
                    if (registro) {
                        out.println("\nCuenta creada con éxito\n");
                    }
                } else {
                    out.println("\nEl saldo inicial debe ser mayor a 50.000");
                    out.println("\nCuenta no creada\n");
                }
            }else if(tipoCuenta == 3){
                out.println("\nEsta cuenta requiere asociar una cuenta corriente");
                out.println("\nPor favor, digite el numero de la cuenta a la que desea asociar: ");
                int idCuenta = Integer.parseInt(in.readLine());
                if(gestor.comprobarCuentaAhorroProgramada(idCuenta)){
                    out.println("\nDigite el saldo inicial de la cuenta:");
                    double saldo = Double.parseDouble(in.readLine());
                    if (saldo >= 50000) {
                        out.println("\nDigite el monto que desea programar:");
                        double monto = Double.parseDouble(in.readLine());
                        out.println("\nDigite el dia del mes en que quiere programar el ahorro (dd):");
                        int dia = Integer.parseInt(in.readLine());
                        boolean registro = gestor.registrarCuenta(idCliente, saldo, tipoCuenta, monto, dia, idCuenta);
                        if (registro) {
                            out.println("\nCuenta creada con éxito\nAsociada a la cuenta " + idCuenta);
                        }
                    } else {
                        out.println("\nEl saldo inicial debe ser mayor a 50.000");
                        out.println("\nCuenta no creada\n");
                    }
                }
            }
            else{out.println("\nTipo de cuenta no válido");}
        }
        else{ out.println("\nEl cliente no existe");}
    }

    /**
     * Rutina para realizar un depósito
     * @throws IOException excepción de entrada y salida
     */
    public static void realizarDeposito() throws IOException{
        out.println("\nRealizar depósito");
        out.println("================");
        out.println("\nDigite el número de cuenta a la cual desea depositar fondos:");
        int numeroCuenta = Integer.parseInt(in.readLine());
        if(gestor.existeCuenta(numeroCuenta)) {
            if (gestor.tipoCuenta(numeroCuenta).equals("Cuenta Corriente") || gestor.tipoCuenta(numeroCuenta).equals("Ahorro")) {
                out.println("\nDigite el monto a depositar:");
                double monto = Double.parseDouble(in.readLine());
                if (monto > 0) {
                    out.println("\nDigite la descripción del depósito:");
                    String descripcion = in.readLine();
                    gestor.depositar(numeroCuenta, monto, descripcion);
                    out.println("\nDepósito realizado con éxito\n");
                } else {
                    out.println("\nOperación No realizada!\nEl monto a depositar no puede ser 0 ni negativo");
                }
            }else {out.println("\nEn las cuentas de ahorro programado no se pueden realizar depósitos");}
        }else{ out.println("\nLa cuenta no existe");}
    }

    /**
     * Rutina para realizar un retiro
     * @throws IOException excepción de entrada y salida
     */
    public static void realizarRetiro() throws IOException{
        out.println("Realizar retiro");
        out.println("================");
        out.println("\nDigite el número de cuenta de la cual desea retirar fondos:");
        int numeroCuenta2 = Integer.parseInt(in.readLine());
        if(gestor.existeCuenta(numeroCuenta2)) {
            ArrayList<Cuenta> cuentas = gestor.listarCuentas();
            double saldo = 0;
            LocalDate fecha = null;
            String tipoCuenta = gestor.tipoCuenta(numeroCuenta2);
            for (Cuenta cuenta : cuentas) {
                if (cuenta.getNumeroCuenta() == numeroCuenta2) {
                    saldo = cuenta.getSaldo();
                    fecha = cuenta.getFechaApertura();
                }
            }
            out.println("\nDigite el motivo del retiro:");
            String motivo = in.readLine();
            switch (tipoCuenta) {
                case "Cuenta Corriente":
                    out.println("\nDigite el monto a retirar:");
                    double monto = Double.parseDouble(in.readLine());
                    if (monto <= saldo && monto > 0) {
                        gestor.retirar(numeroCuenta2, monto, motivo);
                        out.println("\nRetiro realizado con éxito\n");
                    } else {
                        out.println("\nOperación No realizada!\nEl monto a retirar no puede ser ni 0, ni negativo ni mayor al saldo actual");
                    }
                    break;
                case "Ahorro":
                    out.println("\nDigite el monto a retirar:");
                    double monto2 = Double.parseDouble(in.readLine());
                    if (saldo > 100000 && monto2 <= saldo / 2 && monto2 > 0) {
                        gestor.retirar(numeroCuenta2, monto2, motivo);
                        out.println("\nRetiro realizado con éxito\n");
                    } else {
                        out.println("\nOperación No realizada!\nEl saldo de su cuenta es inferior a 100.000 colones o El monto a retirar es 0, negativo o mayor al 50% saldo actual");
                    }
                    break;
                case "Ahorro Programado":
                    if (LocalDate.now().isAfter(fecha.plusDays(365))) {
                        out.println("\nDigite el monto a retirar:");
                        double monto3 = Double.parseDouble(in.readLine());
                        if (monto3 <= saldo && monto3 > 0) {
                            gestor.retirar(numeroCuenta2, monto3, motivo);
                            out.println("\nRetiro realizado con éxito\n");
                        } else {
                            out.println("\nOperación No realizada!\nEl monto a retirar no puede ser ni 0, ni negativo ni mayor al saldo actual");
                        }
                    }
                    else{ out.println("\nNo puede efectuar retiro porque su cuenta tiene menos de 1 año de aperturada:");}
                    break;
            }
        } else{ out.println("\nLa cuenta no existe");}
    }

    /**
     * Rutina para mostrar el saldo de una cuenta
     * @throws IOException excepcion de entrada y salida
     */
    public static void mostrarCuentas() throws IOException{
        out.println("\nSaldo de cuentas");
        out.println("==================\n");
        out.println("Digite el número de cuenta que desea consultar:");
        int numeroCuenta = Integer.parseInt(in.readLine());
        if(gestor.existeCuenta(numeroCuenta)) {
            ArrayList<Cuenta> cuentas = gestor.listarCuentas();
            for (Cuenta cuenta : cuentas) {
                if(cuenta.getSaldo()==0){
                    out.println("\nLa cuenta no tiene fondos disponibles");
                }
                else if(cuenta.getNumeroCuenta()==numeroCuenta){
                    out.println("\nSaldo actual: " + cuenta.getSaldo() + "\n");
                }
            }
        }
        else{ out.println("\nLa cuenta no existe");}
    }
}

